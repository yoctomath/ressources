Lorsqu'on utilise une unité de mesure (par exemple le \textbf{mètre}, noté \textbf{m}, pour mesure des longueur), on besoin~:
\begin{itemize}
  \item de \textbf{multiples}, c'est-à-dire une certain nombre de fois une unité entière~:
        \begin{itemize}
          \item le \textbf{décamètre} (noté \textbf{dam}) qui représente $10$ mètres~;
          \item l'\textbf{hectomètre} (noté \textbf{hm}) qui représente $100$ mètres~;
          \item le \textbf{kilomètre} (noté \textbf{km}) qui représente $\num{1000}$ mètres~;
        \end{itemize}
  \item de \textbf{sous-multiples}, c'est-à-dire des morceaux d'unité entière~:
        \begin{itemize}
          \item le \textbf{décimètre} (noté \textbf{dm}) qui représente $\frac{1}{10}$\ieme{} de mètre
                ($1$ mètre partagé en $10$)~;
          \item le \textbf{centimètre} (noté \textbf{cm}) qui représente $\frac{1}{100}$\ieme{} de mètre
                ($1$ mètre partagé en $100$)~;
          \item le \textbf{millimètre} (noté \textbf{mm}) qui représente $\frac{1}{\num{1000}}$\ieme{} de mètre
                ($1$ mètre partagé en $\num{1000}$).
        \end{itemize}
\end{itemize}

\subsection*{Comment passer d'une unité à une autre ?}

Je souhaite transformer $\Lg{7831,9}$ en décamètre (noté dam).

\begin{center}
  \begin{tikzpicture}[
      unite/.style={inner sep=5pt,fill=black!20,draw,circle,font=\Large,minimum width=2cm},
      comment/.style={text width=2cm,align=center, inner sep=0pt}]
    \node[unite] (cm) at (0,0) {cm};
    \node[unite] (dam) at (6,0) {dam};
    \draw[line width=5pt,-{Triangle[]},black!20] (cm) -- (dam);
    \node[comment,below=5mm of cm] {Unité de départ};
    \node[comment,below=5mm of dam] {Unité cible};
  \end{tikzpicture}
\end{center}
\bigskip
On procède en quatre étapes dans un tableau~:


\begin{multicols}{4}\small
  \paragraph{Étape 1}
  On repère le chiffre des unités dans le nombre~:
  \begin{center}
    {\Large 7\,83\tikz[remember picture,anchor=base,baseline]{\node[inner sep=0pt,fill=colorSec!30] (unite) {1};},9}\bigskip

    \begin{tikzpicture}[remember picture, overlay]
      \node (comment) {Chiffre des unités};
      \draw[-latex'] (comment) -- (unite);
    \end{tikzpicture}
  \end{center}
  \columnbreak

  \paragraph{Étape 2}
  On positionne le chiffre des unités dans la colonne de l'unité de départ (ici la colonne des centimètres)~:
  \begin{center}
    \begin{tblr}{width=\linewidth,columns={4.5mm,c,leftsep=1pt,rightsep=1pt},vlines,hlines,column{6}={bg=colorSec!30},row{1}={font=\tiny}}
      km & hm & dam & m & dm & cm         & mm \\
         &    &     &   &    & {\large 1} &    \\
    \end{tblr}
  \end{center}
  \columnbreak

  \paragraph{Étape 3} On écrit les autres chiffres du nombre, \textbf{mais pas la virgule~!}~:
  \begin{center}
    \begin{tblr}{width=\linewidth,columns={4.5mm,c,leftsep=1pt,rightsep=1pt},vlines,hlines,row{1}={font=\tiny},row{2}={font=\large}}
      km & hm & dam & m & dm & cm & mm \\
         &    & 7   & 8 & 3  & 1  & 9  \\
    \end{tblr}
  \end{center}
  \columnbreak

  \paragraph{Étape 4} On positionne la virgule dans la colonne de l'unité cible (ici le décamètre, noté dam)~:
  \begin{center}
    \begin{tblr}{width=\linewidth,columns={4.5mm,c,leftsep=1pt,rightsep=1pt},vlines,hlines,row{1}={font=\tiny},row{2}={font=\large},column{3}={bg=colorSec!30}}
      km & hm & dam & m & dm & cm & mm \\
         &    & 7,  & 8 & 3  & 1  & 9  \\
    \end{tblr}
  \end{center}
\end{multicols}\bigskip

On a donc $\Lg{7831,9}=\Lg[dam]{7,8319}$.

\subsection*{Application}
Compléter les égalités suivantes~:
\begin{align*}
  \Lg{700}     & = \ldots\text{m}   & \Lg[mm]{2}    & = \ldots\text{m}  & \Lg[hm] {80}  & = \ldots\text{km} & \Lg[km]{503}    & = \ldots\text{dam} \\
  \Lg{90}      & = \ldots\text{m}   & \Lg[dm]{54}   & = \ldots\text{cm} & \Lg[dam]{781} & = \ldots\text{hm} & \Lg[dm]{640}    & = \ldots\text{hm}  \\
  \\
  \Lg[m] {0,3} & = \ldots\text{dm}  & \Lg[dm]{87,4} & = \ldots\text{hm} & \Lg[dm]{6,91} & = \ldots\text{hm} & \Lg[dam]{16,53} & = \ldots\text{km}  \\
  \Lg{0,04}    & = \ldots\text{dam} & \Lg[mm]{0,97} & = \ldots\text{cm} & \Lg[dam]{3,9} & = \ldots\text{dm} & \Lg[dam]{1,84}  & = \ldots\text{km}
\end{align*}

\newpage

\begin{tblr}{width=\linewidth,colspec={*{7}{X[c,1]}},stretch=2,vlines,row{1}={font=\bfseries,bg=colorPrim,fg=white},row{2,3}={bg=colorPrim!10},row{3}={font=\scriptsize}}
  \hline
  \SetCell[c=3]{c} Multiples &                          &                         & U.I.             & \SetCell[c=3]{c} Sous-multiples &                      &                             \\
  \hline
  km                         & hm                       & dam                     & m                & dm                              & cm                   & mm                          \\
  $\ldots\times \num{1000}$  & $\ldots\times \num{100}$ & $\ldots\times \num{10}$ & $\ldots\times 1$ & $\frac{\ldots}{10}$             & $\frac{\ldots}{100}$ & $\frac{\ldots}{\num{1000}}$ \\
  \hline
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
                             &                          &                         &                  &                                 &                      &                             \\
\end{tblr}