chiffres = [x for x in range(1,10)]
nombres = ["{}{}".format(x,y) for x in chiffres for y in chiffres]

def produit(nb):
  return int(nb[:1]) * int(nb[1:])

def chaine(nb, result):
  p = str(produit(nb))
  result.append(p)
  if len(p) > 1:
    return chaine(p,result)
  else:
    return result

chaines = [chaine(x,[x]) for x in nombres]
plus_longues = []
longueur_max = 0
for c in chaines:
  if len(c) > longueur_max:
    longueur_max = len(c)
    plus_longues = []
    plus_longues.append(c)
  else:
    if len(c) == longueur_max:
      plus_longues.append(c)
print("liste des chaines : ", chaines)
print("les plus longues : ", plus_longues)