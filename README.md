# Ressources en Mathématiques pour le Collège

## Objectifs

Regrouper des ressources en mathématiques $\\LaTeX$ à destination des élèves de collège. Ces ressources peuvent être :

* des exercices ;
* des activités de découverte ;
* des tâches à prise d'initiative ;
* etc.

Les ressources sont organisées par niveaux puis par thèmes.